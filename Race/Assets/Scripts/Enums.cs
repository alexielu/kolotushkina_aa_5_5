﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
   // Стадия игры
   public enum GameState : byte
   {
      PreStart = 0,
      Race = 1,
      Results = 2
   }

   // Настройки скина машины
   public enum CarColor : byte
   {
      Red = 0,
      Yellow = 1,
      Black = 2
   }

   // Настройка привода машины
   public enum DriveType : byte
   {
      Front = 0,
      Rear = 1
   }

   [Serializable]
   public struct ResultLine // Структура для работы с таблицей рекордов - результат игрока
   {
      public string Name;
      public TimeSpan Time;

      public ResultLine(string name, TimeSpan time)
      {
         Name = name;
         Time = time;
      }
   }

   [Serializable]
   public struct CarCustomization // Структура для настройки материалов машины
   {
      public CarColor ColorID;
      public Material Material;
   }
}