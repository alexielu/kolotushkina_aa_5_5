﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Race
{
    public class SettingsManager : MonoBehaviour
    {
        // Путь к файлу с записью настроек
        private string _settingsPath = Environment.CurrentDirectory + "\\Settings.txt";

        [Header("Default Settings")] // Изначальные настройки машины
        [SerializeField] private float _mass = 1500;  // Масса машины (помещается в Rigidbody)
        [SerializeField] private float _angle = 25; // Max Steer Angle в компоненте Car
        [SerializeField] private float _engine = 2500; // Torque в компоненте  Car
        [SerializeField] private CarColor _color = CarColor.Red; // Скин машины
        [SerializeField] private DriveType _drive = DriveType.Rear; // Тип привода

        [Header("Settings")] // Ссылки на интерфейсные элементы
        [SerializeField] private Slider _massSlider;
        [SerializeField] private Slider _angleSlider;
        [SerializeField] private Slider _engineSlider;
        [SerializeField] private TMP_Dropdown _colorDrop;
        [SerializeField] private TMP_Dropdown _driveDrop;

        [Header("Car Visual")]
        // Комплект машин разных цветов, можно было оформить через обновление материала, но так было быстрее :)
        // (материал меняется в самом уровне) 
        [SerializeField] private GameObject[] _cars; 
        private int _currentCar; // id текущей машины

        void Start()
        {
            // Загрузка ранее сохраненных настроек при наличии файла
            if (File.Exists(_settingsPath))
            {
                using (var reader = new StreamReader(_settingsPath))
                {
                    string line;
                    if ((line = reader.ReadLine()) != null)
                    {
                        string[] data = line.Split('_');

                        _massSlider.value = float.Parse(data[0]);
                        _angleSlider.value = float.Parse(data[1]);
                        _engineSlider.value = float.Parse(data[2]);
                        
                        _colorDrop.value = int.Parse(data[3]);
                        ChangeCarColor();
                        
                        _driveDrop.value = int.Parse(data[4]);
                    }
                }
            }
            else
            {
                // Если файла нет, то сбрасываем в дефолтные настройки
                ResetSettings();
            }
        }
        
        public void SaveSettings() // Сохранение настроек
        {
            // Создание файла при необходимости
            if (!File.Exists(_settingsPath))
            {
                var newFile = File.Create(_settingsPath);
                newFile.Close();
            }

            // Запись в файл
            string settings =
                $"{_massSlider.value}_{_angleSlider.value}_{_engineSlider.value}_{_colorDrop.value}_{_driveDrop.value}";
            
            File.WriteAllText(_settingsPath, settings);
        }

        public void ResetSettings() // Сброс до дефолтных значений
        {
            _massSlider.value = _mass;
            _angleSlider.value = _angle;
            _engineSlider.value = _engine;
            _colorDrop.value = (int) _color;
            _driveDrop.value = (int) _drive;
                
            SaveSettings();
        }

        public void ChangeCarColor() // Обновление машины при смене настройки цвета
        {
            _cars[_currentCar].SetActive(false);
            _currentCar = _colorDrop.value;
            _cars[_currentCar].SetActive(true);
        }

        public void BackToMenu() // Переход в стартовое меню
        {
            SaveSettings();
            SceneManager.LoadScene("Menu");
        }
    }
}
