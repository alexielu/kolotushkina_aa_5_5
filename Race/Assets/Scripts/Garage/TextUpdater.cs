﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Race
{
    // Вспомогательный класс для визуализации текущего значения выбранного слайдера
    public class TextUpdater : MonoBehaviour 
    {
        [SerializeField] private TextMeshProUGUI _text; // Текст с текущим значением

        public void UpdateText()
        {
            _text.text = GetComponent<Slider>().value.ToString();
        }
    }
}