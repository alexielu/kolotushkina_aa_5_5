﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
    public class CarRotation : MonoBehaviour // Класс, отвечающий за вращение подиума с машиной
    {
        private CarControls _controls;
        private bool _rotateCar;
        [SerializeField] private float _rotationSpeed; // Скорость вращения машины

        void Awake()
        {
            _controls = new CarControls();
        }

        void OnEnable()
        {
            _controls.Car.Enable();
            
            _controls.Car.RotateCar.performed += _ =>
            {
                // Избегаем вращения машины во время использования настроек в интерфейсе
                if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    _rotateCar = true;
            };
            _controls.Car.RotateCar.canceled += _ =>
            {
                _rotateCar = false;
            };
        }

        void Update()
        {
            if (_rotateCar)
            {
                var rot = _controls.Car.MouseX.ReadValue<float>() * _rotationSpeed * Mathf.Deg2Rad;
                transform.RotateAround(Vector3.up, -rot);  
            }
        }
        
        void OnDisable()
        {
            _controls.Car.Disable();
        }

        void OnDestroy()
        {
            _controls.Dispose();
        }
    }
}