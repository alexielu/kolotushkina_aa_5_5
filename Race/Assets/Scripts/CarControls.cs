// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/CarControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Race
{
    public class @CarControls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @CarControls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""CarControls"",
    ""maps"": [
        {
            ""name"": ""Car"",
            ""id"": ""ddd4147a-3b55-4e40-961e-c67f97f31fe5"",
            ""actions"": [
                {
                    ""name"": ""HandBrake"",
                    ""type"": ""Button"",
                    ""id"": ""f624bde6-547d-4d55-8869-573fe1a44a23"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Acceleration"",
                    ""type"": ""Value"",
                    ""id"": ""8616906d-ee58-4c42-9e49-dd1e3da0a550"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Rotation"",
                    ""type"": ""Value"",
                    ""id"": ""2ca05e44-c92e-4aa4-b4e3-8f1e90e165d5"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MouseX"",
                    ""type"": ""Value"",
                    ""id"": ""fab397cd-d053-41b8-b694-b3f2a5f7a0cc"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RotateCar"",
                    ""type"": ""Button"",
                    ""id"": ""f42e0ac1-5904-4320-8a0e-c520ea178d9a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4a36f484-84a0-4c94-969a-0de5a040d460"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HandBrake"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WS"",
                    ""id"": ""8f6f5678-daa3-4dbc-834c-d974f6713195"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acceleration"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""4fe726d4-bef1-44ae-8e4c-b304b37ec05c"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acceleration"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""5aa617c9-e227-484a-99e4-5a7c1249c160"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Acceleration"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""AD"",
                    ""id"": ""0aa5f7a4-65d4-446b-b612-1d8423845aa7"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotation"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""a62b7476-bde0-4710-990a-75d91a9984ca"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""2b376422-549f-4bd6-b154-8f4d3f55d880"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""eceea488-0de7-4547-9d06-607efbe640bc"",
                    ""path"": ""<Mouse>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5ada5b0b-ff3a-4cbf-b530-53ba8764064d"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RotateCar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Car
            m_Car = asset.FindActionMap("Car", throwIfNotFound: true);
            m_Car_HandBrake = m_Car.FindAction("HandBrake", throwIfNotFound: true);
            m_Car_Acceleration = m_Car.FindAction("Acceleration", throwIfNotFound: true);
            m_Car_Rotation = m_Car.FindAction("Rotation", throwIfNotFound: true);
            m_Car_MouseX = m_Car.FindAction("MouseX", throwIfNotFound: true);
            m_Car_RotateCar = m_Car.FindAction("RotateCar", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Car
        private readonly InputActionMap m_Car;
        private ICarActions m_CarActionsCallbackInterface;
        private readonly InputAction m_Car_HandBrake;
        private readonly InputAction m_Car_Acceleration;
        private readonly InputAction m_Car_Rotation;
        private readonly InputAction m_Car_MouseX;
        private readonly InputAction m_Car_RotateCar;
        public struct CarActions
        {
            private @CarControls m_Wrapper;
            public CarActions(@CarControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @HandBrake => m_Wrapper.m_Car_HandBrake;
            public InputAction @Acceleration => m_Wrapper.m_Car_Acceleration;
            public InputAction @Rotation => m_Wrapper.m_Car_Rotation;
            public InputAction @MouseX => m_Wrapper.m_Car_MouseX;
            public InputAction @RotateCar => m_Wrapper.m_Car_RotateCar;
            public InputActionMap Get() { return m_Wrapper.m_Car; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(CarActions set) { return set.Get(); }
            public void SetCallbacks(ICarActions instance)
            {
                if (m_Wrapper.m_CarActionsCallbackInterface != null)
                {
                    @HandBrake.started -= m_Wrapper.m_CarActionsCallbackInterface.OnHandBrake;
                    @HandBrake.performed -= m_Wrapper.m_CarActionsCallbackInterface.OnHandBrake;
                    @HandBrake.canceled -= m_Wrapper.m_CarActionsCallbackInterface.OnHandBrake;
                    @Acceleration.started -= m_Wrapper.m_CarActionsCallbackInterface.OnAcceleration;
                    @Acceleration.performed -= m_Wrapper.m_CarActionsCallbackInterface.OnAcceleration;
                    @Acceleration.canceled -= m_Wrapper.m_CarActionsCallbackInterface.OnAcceleration;
                    @Rotation.started -= m_Wrapper.m_CarActionsCallbackInterface.OnRotation;
                    @Rotation.performed -= m_Wrapper.m_CarActionsCallbackInterface.OnRotation;
                    @Rotation.canceled -= m_Wrapper.m_CarActionsCallbackInterface.OnRotation;
                    @MouseX.started -= m_Wrapper.m_CarActionsCallbackInterface.OnMouseX;
                    @MouseX.performed -= m_Wrapper.m_CarActionsCallbackInterface.OnMouseX;
                    @MouseX.canceled -= m_Wrapper.m_CarActionsCallbackInterface.OnMouseX;
                    @RotateCar.started -= m_Wrapper.m_CarActionsCallbackInterface.OnRotateCar;
                    @RotateCar.performed -= m_Wrapper.m_CarActionsCallbackInterface.OnRotateCar;
                    @RotateCar.canceled -= m_Wrapper.m_CarActionsCallbackInterface.OnRotateCar;
                }
                m_Wrapper.m_CarActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @HandBrake.started += instance.OnHandBrake;
                    @HandBrake.performed += instance.OnHandBrake;
                    @HandBrake.canceled += instance.OnHandBrake;
                    @Acceleration.started += instance.OnAcceleration;
                    @Acceleration.performed += instance.OnAcceleration;
                    @Acceleration.canceled += instance.OnAcceleration;
                    @Rotation.started += instance.OnRotation;
                    @Rotation.performed += instance.OnRotation;
                    @Rotation.canceled += instance.OnRotation;
                    @MouseX.started += instance.OnMouseX;
                    @MouseX.performed += instance.OnMouseX;
                    @MouseX.canceled += instance.OnMouseX;
                    @RotateCar.started += instance.OnRotateCar;
                    @RotateCar.performed += instance.OnRotateCar;
                    @RotateCar.canceled += instance.OnRotateCar;
                }
            }
        }
        public CarActions @Car => new CarActions(this);
        public interface ICarActions
        {
            void OnHandBrake(InputAction.CallbackContext context);
            void OnAcceleration(InputAction.CallbackContext context);
            void OnRotation(InputAction.CallbackContext context);
            void OnMouseX(InputAction.CallbackContext context);
            void OnRotateCar(InputAction.CallbackContext context);
        }
    }
}
