﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Race
{
    public class Speedometer : MonoBehaviour
    {
        private const float c_ConvertMeterInSecFromKmInHour = 3.6f;
        
        [SerializeField] private Transform _player;
        [SerializeField] private float _minSpeed = 0f;
        [SerializeField] private float _maxSpeed = 180f;
        [SerializeField, Min(0f)] private float _delay = .3f;
        
        [Header("Text Version")]
        [SerializeField] private Color _minColor = Color.white;
        [SerializeField] private Color _maxColor = Color.red;
        [SerializeField] private TextMeshProUGUI _text;

        [Header("Image Version")] // Визуальная версия спидометра
        [SerializeField] private Transform _tick; // Стрелка 
        [SerializeField] private float _tickSpeed = 1.5f; // Скорость поворота стрелки
        private Vector3 _angle = new Vector3(0f, 0f, 90f); // Поворот стрелки
        
        void Start()
        {
            StartCoroutine(SpeedCalculation());
        }

        void Update()
        {
            // Вращение стрелки
            _tick.rotation = Quaternion.Lerp(_tick.rotation, Quaternion.Euler(_angle), Time.deltaTime * _tickSpeed);
        }
        
        private IEnumerator SpeedCalculation() // Расчет скорости
        {
            var prePos = _player.position;
            while (true)
            {
                var distance = Vector3.Distance(prePos, _player.position);
                var speed = (float) System.Math.Round(distance / _delay * c_ConvertMeterInSecFromKmInHour, 1);
                
                // Text
                _text.text = speed.ToString("F1");
                _text.color = Color.Lerp(_minColor, _maxColor, speed / _maxSpeed);
                
                // Расчет угла поворота стрелки
                _angle = new Vector3(0f, 0f, Mathf.Lerp(90f, -90f, speed / _maxSpeed));
                
                prePos = _player.position;
                yield return new WaitForSeconds(_delay);
            }
        }
    }
}