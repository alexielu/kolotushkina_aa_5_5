﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
    public class PlayerInputController : BaseInputController
    {
        private CarControls _controls;

        #region EnableControls

        void Awake()
        {
            _controls = new CarControls();
        }

        void OnEnable()
        {
            _controls.Car.Enable();
        }

        #endregion
        
        protected override void Start()
        {
            base.Start();

            _controls.Car.HandBrake.performed += _ => CallHandBrake(true);
            _controls.Car.HandBrake.canceled += _ => CallHandBrake(false);
        }

        protected override void FixedUpdate()
        {
            // Ограничение движения исходя из стадии игры (Game State контролирует Allow Movement)
            if (!GameController.Instance.AllowMovement)
            {
                if (Acceleration != 0)
                {
                    Acceleration = 0;
                    Rotation = 0;
                    CallHandBrake(true);
                }
                return;
            }
            
            // Поворот
            var direction = _controls.Car.Rotation.ReadValue<float>();
            if (direction == 0f && Rotation != 0f)
            {
                Rotation = Rotation < 0f
                    ? Rotation + Time.fixedDeltaTime
                    : Rotation - Time.fixedDeltaTime;
            }
            else
                Rotation = Mathf.Clamp(Rotation + direction * Time.fixedDeltaTime, -1f, 1f);

            Acceleration = _controls.Car.Acceleration.ReadValue<float>();
        }

        #region DisableControls

        void OnDisable()
        {
            _controls.Car.Disable();
        }

        void OnDestroy()
        {
            _controls.Dispose();
        }

        #endregion
    }
}