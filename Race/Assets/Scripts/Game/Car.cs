﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
    [RequireComponent(typeof(BaseInputController), typeof(Wheels), typeof(Rigidbody))]
    public class Car : MonoBehaviour
    {
        private BaseInputController _input;
        private Wheels _wheels;
        private Rigidbody _rb;
        private bool _frontWheelDrive; // Привод

        [SerializeField, Min(1f)]
        private float _torque = 2500f;
        [SerializeField, Range(5f, 40f)]
        private float _maxSteerAngle = 25f;
        [SerializeField, Range(0f, float.MaxValue)]
        private float _brakeTorque = float.MaxValue;
        [SerializeField] private Vector3 _centerOfMass;

        [Header("Visual")]
        [SerializeField] private MeshRenderer _renderer;

        void Start()
        {
            _input = GetComponent<BaseInputController>();
            _wheels = GetComponent<Wheels>();
            _rb = GetComponent<Rigidbody>();
            _rb.centerOfMass = _centerOfMass;
            
            _input.OnHandBrake += OnHandBrake;
        }

        // Установка параметров из настроек
        public void SetParameters(float mass, float maxAngle, float engine, CarCustomization customization, int driveID)
        {
            _rb.mass = mass;
            _maxSteerAngle = maxAngle;
            _torque = engine;
            _renderer.material = customization.Material;
            _frontWheelDrive = (DriveType) driveID == DriveType.Front;
        }

        private void FixedUpdate()
        {
            OnDrive();
            OnRotate();
        }

        private void OnDrive()
        {
            var torque = _input.Acceleration * _torque / 2f;
            
            // Добавляем учет типа привода машины 
            foreach (var wheel in _frontWheelDrive ? _wheels.GetFrontWheelColliders : _wheels.GetRearWheelColliders)
            {
                wheel.motorTorque = torque;
            }
        }

        private void OnRotate()
        {
            _wheels.UpdateVisual(_input.Rotation * _maxSteerAngle);
        }

        private void OnHandBrake(bool isHandBrake)
        {
            if (isHandBrake)
            {
                foreach (var wheel in _wheels.GetRearWheelColliders)
                {
                    wheel.brakeTorque = _brakeTorque;
                    wheel.motorTorque = 0f;
                }
            }
            else
            {
                foreach (var wheel in _wheels.GetRearWheelColliders)
                    wheel.brakeTorque = 0f;
            }
        }
        
        void OnDestroy()
        {
            _input.OnHandBrake -= OnHandBrake;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.TransformPoint(_centerOfMass), .5f);
        }
    }
}