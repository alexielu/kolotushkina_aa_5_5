﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
    public abstract class BaseInputController : MonoBehaviour
    {
        public float Acceleration { get; protected set; }
        public float Rotation { get; protected set; }
        public Action<bool> OnHandBrake;

        protected virtual void Start() { }
        protected abstract void FixedUpdate();
        protected void CallHandBrake(bool value) => OnHandBrake?.Invoke(value);
    }
}