﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
    public class TraceMarker : MonoBehaviour
    {
        void Start()
        {
            GetComponent<SphereCollider>().isTrigger = true;
        }

        void OnTriggerEnter(Collider other) // Триггер обновления маркера у бота
        {
            if (other.CompareTag("Bot"))
                other.GetComponentInParent<BotInputController>().NextMarker();
        }
    }
}