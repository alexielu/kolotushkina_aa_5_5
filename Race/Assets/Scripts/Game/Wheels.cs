﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Race
{
    public class Wheels : MonoBehaviour
    {
        [SerializeField] private Transform _leftRearMesh;
        [SerializeField] private Transform _rightRearMesh;
        [SerializeField] private Transform _leftFrontMesh;
        [SerializeField] private Transform _rightFrontMesh;
        
        [Space]
        [SerializeField] private WheelCollider _leftRearCollider;
        [SerializeField] private WheelCollider _rightRearCollider;
        [SerializeField] private WheelCollider _leftFrontCollider;
        [SerializeField] private WheelCollider _rightFrontCollider;

        private Transform[] _frontMeshes;
        private Transform[] _rearMeshes;
        private WheelCollider[] _frontColliders;
        private WheelCollider[] _rearColliders;

        public WheelCollider[] GetFrontWheelColliders => _frontColliders;
        public WheelCollider[] GetRearWheelColliders => _rearColliders;
        public WheelCollider[] GetAllWheelColliders => _frontColliders.Concat(_rearColliders).ToArray();

        private void Start()
        {
            _frontMeshes = new Transform[] {_leftFrontMesh, _rightFrontMesh};
            _rearMeshes = new Transform[] {_leftRearMesh, _rightRearMesh};
            
            _frontColliders = new WheelCollider[] {_leftFrontCollider, _rightFrontCollider};
            _rearColliders = new WheelCollider[] {_leftRearCollider, _rightRearCollider};
        }

        public void UpdateVisual(float angle)
        {
            for (int i = 0; i < _frontMeshes.Length; i++)
            {
                _frontColliders[i].steerAngle = angle;
                
                _frontColliders[i].GetWorldPose(out var position, out var rotation);
                _frontMeshes[i].position = position;
                _frontMeshes[i].rotation = rotation;
                
                _rearColliders[i].GetWorldPose(out position, out rotation);
                _rearMeshes[i].position = position;
                _rearMeshes[i].rotation = rotation;
            }
        }
    }
}