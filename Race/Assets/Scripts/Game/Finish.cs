﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
    public class Finish : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other) // Триггер фиксирования результата для игрока (бот не учитывается)
        {
            if (other.CompareTag("Player"))
                GameController.Instance.ChangeState(GameState.Results);
        }
    }
}