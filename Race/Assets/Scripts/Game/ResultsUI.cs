﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Race
{
    public class ResultsUI : MonoBehaviour
    {
        [SerializeField] private GameObject _resultPanel; // Панель с результатами
        [SerializeField] private GameObject[] _buttons; // Кнопки выхода и запуска новой попытки заезда
        [SerializeField] private TextMeshProUGUI[] _resultText; // Массив текста таблицы рекордов
        [SerializeField] private TMP_InputField _inputName; // Поле ввода ника игрока
        [SerializeField] private TextMeshProUGUI _currentTime; // Текущий результат заезда

        private bool _updatedLeaderboard; // Переменная для избежания повторной записи результата
        
        public void OpenWindow(ResultLine[] results, string currentTime) // Открытие панели результатов
        {
            _updatedLeaderboard = false; // Сброс записи данных

            for (int i = 0; i < results.Length; i++) // Обновление текста таблицы
            {
                var name = results[i].Name == "" ? "Player" : results[i].Name;
                _resultText[i].text = $"{i + 1}. {name} " + results[i].Time.ToString(@"mm\:ss\:ff");
            }
            
            _currentTime.text = currentTime;
            
            _resultPanel.SetActive(true);
            foreach (var b in _buttons)
                b.SetActive(false);
        }

        public void UpdateLeaderboard() // Обновление таблицы результатов (Запускается при окончании ввода ника игрока)
        {
            if (_updatedLeaderboard) return;
            
            // Получение обновленной таблицы при сохранении данных
            ResultLine[] updatedResults = GameController.Instance.SaveResults(_inputName.text);
            for (int i = 0; i < updatedResults.Length; i++)
                _resultText[i].text = $"{i + 1}. {updatedResults[i].Name} " + updatedResults[i].Time.ToString(@"mm\:ss\:ff");

            _updatedLeaderboard = true;
            foreach (var b in _buttons)
                b.SetActive(true);
        }
        
        // Метод повторного запуска гонки
        // И дополнительно на кнопке рестарта на случай, если машина вылетела с трассы / застряла
        public void Restart() 
        {
            SceneManager.LoadScene("Trace");
        }

        public void Exit() // Выход в меню
        {
            SceneManager.LoadScene("Menu");
        }
    }
}