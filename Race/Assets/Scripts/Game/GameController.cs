﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using TMPro;
using UnityEngine;

namespace Race
{
    public class GameController : MonoBehaviour
    {
        private GameState _state;
        
        // Пути к файлам с сохраненными данными
        private string _path = Environment.CurrentDirectory + "\\Results.txt";
        private string _settingsPath = Environment.CurrentDirectory + "\\Settings.txt";
        private ResultLine[] _results = new ResultLine[10];

        [SerializeField] private Transform _player; 

        [Header("Trace")]
        [SerializeField] private TraceMarker[] _markers; // Массив маркеров трассы
        public TraceMarker[] Markers => _markers;
        
        [Header("Results")]
        [SerializeField] private ResultsUI _resultsUI; // Контроллер панели интерфейса с результатами
        
        [Header("Countdown")]
        [SerializeField] private TextMeshProUGUI _countdown; // Текст обратного отсчета
        [SerializeField] private Animation _anim; // Анимация обратного отсчета
        
        [Header("Timer")]
        [SerializeField] private TextMeshProUGUI _timerText; // Текст таймера заезда
        private float _timer; // Сам таймер

        [Header("Customization")]
        [SerializeField] private CarCustomization[] _customizations; // Массив скинов машины (материалы)
        
        // Свойство для ограничения передвижения машин во время отсчета и при отображении результатов
        public bool AllowMovement { get; set; } 

        public static GameController Instance;

        void Awake()
        {
            if (Instance != null)
                Destroy(gameObject);
            else
                Instance = this;
        }
        
        void Start()
        {
            ChangeState(GameState.PreStart);
        }

        public void ChangeState(GameState state) // Смена стадии игры
        {
            _state = state;
            switch (_state)
            {
                case GameState.PreStart: // Обратный отсчет
                    AllowMovement = false;
                    if (_player.gameObject.activeSelf) SetAuto();
                    StartCoroutine(Countdown());
                    break;
                
                case GameState.Race: // Гонка
                    AllowMovement = true;
                    StartCoroutine(Timer());
                    break;
                
                case GameState.Results: // Демонстрация результатов
                    AllowMovement = false;
                    LoadResults();
                    OpenResultsMenu();
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetAuto() // Установка настроек машины
        {
            if (File.Exists(_settingsPath))
            {
                using (var reader = new StreamReader(_settingsPath))
                {
                    string line;
                    if ((line = reader.ReadLine()) != null)
                    {
                        string[] data = line.Split('_');

                        _player.GetComponent<Car>().SetParameters(float.Parse(data[0]),float.Parse(data[1]),
                            float.Parse(data[2]), _customizations[int.Parse(data[3])],int.Parse(data[4]));
                    }
                }
            }
        }
        
        private void LoadResults() // Загрузка ранее сохраненных результатов
        {
            if (!File.Exists(_path))
            {
                var newFile = File.Create(_path);
                newFile.Close();
            }
            else
            {
                using (var reader = new StreamReader(_path))
                {
                    string line;
                    int id = 0;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] words = line.Split('_');
                        
                        _results[id].Name = words[0];
                        _results[id].Time = TimeSpan.Parse(words[1]);
                        id++;
                    }
                }
            }
        }

        public ResultLine[] SaveResults(string playerName) // Сохранение результатов с учетом новых данных
        {
            // Фиксируем текущий результат
            var time = TimeSpan.FromSeconds(_timer);
            var newResult = new ResultLine(playerName, time);

            // Определяем его положение в таблице 
            int idToReplace = -1;
            for (int i = 0; i < _results.Length; i++)
            {
                if (time < _results[i].Time || _results[i].Time == TimeSpan.FromSeconds(0))
                {
                    idToReplace = i;
                    break;
                }
            }

            // Если попал в таблицу - перезаписываем данные
            if (idToReplace >= 0)
            {
                for (int i = _results.Length - 1; i > idToReplace; i--)
                {
                    _results[i] = _results[i - 1];
                }
                
                _results[idToReplace] = newResult;
                
                File.WriteAllText(_path, String.Empty);
                using (var writer = new StreamWriter(_path, true))
                {
                    foreach (var line in _results)
                    {
                        writer.Write($"{line.Name}_{line.Time}" + "\n");
                    }
                }
            }

            return _results; // Передаем полученное сохранение в интерфейс
        }
        
        private void OpenResultsMenu() // Открытие панели с результатами
        {
            var time = TimeSpan.FromSeconds(_timer).ToString(@"mm\:ss\:ff");
            _resultsUI.OpenWindow(_results, time);
        }
        
        private IEnumerator Countdown() // Обратный отсчет
        {
            int time = 3;
            _anim.Play("Countdown");

            while (time > 0)
            {
                _countdown.text = time.ToString();
                time--;
                yield return new WaitForSeconds(1f);
            }

            _countdown.text = "start";
            
            ChangeState(GameState.Race);
        }
        
        private IEnumerator Timer() // Время заезда
        {
            _timer = 0f;
            while (_state == GameState.Race)
            {
                _timer += Time.deltaTime;
                TimeSpan time = TimeSpan.FromSeconds(_timer);
                _timerText.text = time.ToString(@"mm\:ss\:ff");
                yield return null;
            }
        }
    }
}