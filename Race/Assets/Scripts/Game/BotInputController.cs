﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Race
{
    public class BotInputController : BaseInputController
    {
        private int _currentMarker; // ID текущего маркера трассы, к которому стремится бот
        private TraceMarker[] _markers; // Массив всех маркеров
        [SerializeField] private float _maxAngle = 25f; // Ограничение поворота бота

        void Start()
        {
            // Обновление маркеров
            _markers = GameController.Instance.Markers;
            _currentMarker = 0;
        }
        
        protected override void FixedUpdate()
        {
            // Ограничение движения исходя из стадии игры (Game State контролирует Allow Movement)
            if (!GameController.Instance.AllowMovement)
            {
                if (Acceleration != 0)
                {
                    Acceleration = 0;
                    CallHandBrake(true);
                }
                return;
            }

            // Определение поворота бота
            var direction = Mathf.Clamp(Angle() / _maxAngle, -1f, 1f);
            if (direction == 0f && Rotation != 0f)
            {
                Rotation = Rotation < 0f
                    ? Rotation + Time.fixedDeltaTime
                    : Rotation - Time.fixedDeltaTime;
            }
            else
            {
                //Rotation = Mathf.Clamp(Rotation + direction * Time.fixedDeltaTime, -1f, 1f);
                
                // Более резкий поворот здесь показывает себя эффективнее
                Rotation = direction; 
            }

            // Не удержалась и настроила использование ускорения,
            // машина замедляется, когда приближается к текущему маркеру, а после смены маркеров снова ускоряется
            var previousIndex = (_currentMarker + _markers.Length - 1) % _markers.Length;
            var distance = (_markers[_currentMarker].transform.position - transform.position).magnitude;
            var markersDistance = (_markers[_currentMarker].transform.position - _markers[previousIndex].transform.position).magnitude;
            Acceleration = _currentMarker == 0 
                ? 1f 
                : Mathf.Lerp(-0.5f, 1f, distance / markersDistance);
            
            //Acceleration = 1;
        }

        private float Angle() // Расчет угла поворота
        {
            var targetPos = _markers[_currentMarker].transform.position;
            targetPos.y = transform.position.y;

            var direction = targetPos - transform.position;
            
            return Vector3.SignedAngle(transform.forward,direction, Vector3.up);
        }

        public void NextMarker() // Обновление маркера трассы
        {
            _currentMarker = (_currentMarker + 1) % _markers.Length;
        }
    }
}