﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Race
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField] private RectTransform[] _buttons; // Кнопки на экране
        [SerializeField] private float _maxButtonPosX = 0f; // Положение завершения анимации кнопок
        [SerializeField] private float _animationTimer; // Длительность анимации

        void Start()
        {
            StartCoroutine(ButtonsAppear());
        }

        public void StartGame()
        {
            SceneManager.LoadScene("Trace");
        }

        public void OpenGarage()
        {
            SceneManager.LoadScene("Garage");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }

        private IEnumerator ButtonsAppear() // Анимация появления кнопок
        {
            var startPosX = _buttons[0].position.x;
            var time = 0f;

            while (time < _animationTimer)
            {
                foreach (var b in _buttons)
                {
                    b.anchoredPosition = Vector2.Lerp(new Vector2(startPosX, b.anchoredPosition.y),
                        new Vector2(_maxButtonPosX, b.anchoredPosition.y), time);
                }

                time += Time.deltaTime;
                yield return null;
            }
        }
    }
}